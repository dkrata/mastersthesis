<?php

require __DIR__ . '/autoload.php';

use mastersthesis\utils\database;
use mastersthesis\models\Article;

$options = array('--mock-database', '--install');

if (count($argv) < 2 || !in_array($argv[1], $options)) {
    echo "Wrong argument passed to script. Should be one of following\n
    " . $options[0] . "\n
    " . $options[1] . "\n";
    return 1;
}

switch ($argv[1]) {
    case $options[0]:
        mock_database($config['database']);
        break;
    case $options[1]:
        install($config['database']);
        break;
}

function install($config)
{
    $dbSchemaCreator = new database\DbSchemaCreator($config);
    $dbSchemaCreator->dropSchema();
    $dbSchemaCreator->createSchema();
    $dbSchemaCreator->createTables();
}

function mock_database($config)
{
    $dbConnection = new database\DbConnectionFactory($config);
    $connection = $dbConnection->getDbConnection();

    $now = new DateTime();

    $content = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus enim sapien, faucibus vel ante a, facilisis hendrerit diam. Cras feugiat, urna et cursus pharetra, libero neque laoreet magna, sit amet hendrerit libero sapien in mauris. Vestibulum nec vehicula odio. Ut mattis sapien massa, eu pellentesque nisi fermentum vitae. Pellentesque consequat eros vitae augue ullamcorper interdum. Integer sit amet erat aliquet, pharetra orci in, ultricies mauris. Pellentesque tincidunt pellentesque bibendum. Morbi posuere arcu vitae pellentesque fringilla. Phasellus iaculis purus sed sollicitudin laoreet. Pellentesque viverra lorem quis quam lacinia fringilla. Duis congue faucibus erat a mollis. Sed ac imperdiet libero.

Sed augue diam, sollicitudin eu urna non, sollicitudin congue purus. Vivamus porta, ligula et fermentum lacinia, purus magna sollicitudin erat, et commodo lectus nulla id purus. Vivamus efficitur mollis orci, a vehicula odio fringilla ac. Sed ullamcorper nibh at mi tempus, at ultricies lacus ornare. Ut maximus eleifend augue vel scelerisque. Proin sit amet elit enim. Mauris interdum lorem id fermentum laoreet. Proin tincidunt quis ante quis maximus. Nam ultricies gravida gravida. Nullam lectus nibh, viverra pellentesque venenatis a, fermentum ut nulla. Phasellus vitae tortor vel nibh suscipit lobortis. Donec dapibus elit eu ligula porttitor, quis facilisis leo rhoncus. Aliquam erat volutpat. Praesent varius justo non iaculis mollis. Morbi accumsan urna scelerisque sem hendrerit congue. Vivamus cursus blandit venenatis.

Aenean sed feugiat est. Aliquam condimentum, nulla a pellentesque dapibus, urna arcu dignissim odio, egestas facilisis metus orci ac sapien. Cras diam metus, vestibulum eget commodo dapibus, ultrices sit amet odio. Donec at rutrum augue, vitae scelerisque enim. Lorem ipsum dolor sit amet, consectetur adipiscing elit. In cursus tortor vitae viverra sagittis. Curabitur sodales facilisis sagittis. Suspendisse maximus nunc id purus vehicula venenatis. Nulla auctor arcu non molestie feugiat. Fusce et magna magna. Mauris condimentum feugiat accumsan.

Aliquam ut pharetra sapien. Nullam ornare velit feugiat euismod semper. Praesent ante urna, sollicitudin eget suscipit eget, commodo sit amet arcu. Nam mollis, justo vel molestie hendrerit, tellus magna posuere est, dapibus facilisis nunc ex vestibulum nibh. Etiam euismod nec eros sit amet volutpat. Ut a mauris vitae velit porttitor feugiat non at ante. Cras volutpat et leo vel sagittis. Praesent imperdiet lorem in placerat convallis. Nulla tempor nunc ex, vel ullamcorper velit laoreet sit amet. Proin consectetur massa id urna aliquam, in vulputate mi scelerisque. Integer congue diam quis enim fringilla mattis. In turpis urna, tincidunt nec diam a, mollis elementum neque. Vestibulum ullamcorper ipsum sed mauris facilisis, non placerat risus semper. Aenean eu sagittis tortor, vel vehicula turpis.

Mauris condimentum accumsan congue. In hac habitasse platea dictumst. Praesent iaculis, mauris vel aliquet imperdiet, justo nunc molestie lacus, id fringilla neque sem a tortor. Proin vitae erat lobortis metus maximus congue sit amet quis lectus. Donec interdum nisl non erat tincidunt rhoncus ac et dui. Proin sed mattis ligula. Nullam id diam pharetra, varius risus eu, fringilla ante. Vestibulum a erat massa. Ut vestibulum metus hendrerit odio condimentum, bibendum aliquet felis placerat. Curabitur ligula arcu, ultricies sed ex eu, ornare vulputate nisi. ";

    $dbMethods = new database\DbMethods($connection);
    $article = new Article();
    $article
        ->setTitle('Title - ' . $now->format('Y-m-d H:i:s'))
        ->setAuthor('Author - ' . $now->format('Y-m-d H:i:s'))
        ->setContent($content . ' - ' . $now->format('Y-m-d H:i:s'));

    $fullContent = $article->getTitle() . $article->getAuthor() . $article->getContent() . "\n\n";

    $size = strlen(base64_encode($fullContent));

    $article->setSize($size);

    $dbMethods->addArticle($article);
}
