<?php

require __DIR__ . "/autoload.php";

$app = new Silex\Application();
$app['debug'] = true;

$dbConnectionFactory =
    new mastersthesis\utils\database\DbConnectionFactory($config['database']);
$connection = $dbConnectionFactory->getDbConnection();
$dbMethods = new mastersthesis\utils\database\DbMethods($connection);

$app->get('/hello/{name}', function ($name) use ($app) {
    return 'Hello ' . $app->escape($name);
});

$articleController = new mastersthesis\controllers\ArticleController($app, $dbMethods);
$articleController->registerControllers();

$app->run();
