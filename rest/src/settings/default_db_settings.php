<?php

/**
 * Db settings will be used in application.
 *
 * Credentials:
 * - root
 *     To create new database and its admin
 * - admin
 *     To service new database
 */
return array(
    'database_name' => '{database.name}',
    'driver' => '{database.driver}',
    'host' => '{database.host}',
    'port' => '{database.port}',
    'credentials' => array(
        'root' => array(
            'username' => '{database.root.username}',
            'password' => '{database.root.password}'
        ),
        'admin' => array(
            'username' => '{database.admin.username}',
            'password' => '{database.admin.password}'
        ),
    )
);
