<?php

define("DIR_ROOT", __DIR__ . "/..");
define("DIR_SETTINGS", __DIR__);
define("DIR_MODELS", DIR_ROOT . "/models");
define("DIR_UTILS", DIR_ROOT . "/utils");
define("DIR_CONTROLLERS", DIR_ROOT . "/controllers");
define("DIR_DATABASE", DIR_SETTINGS . "/database");
define("DIR_VENDOR", DIR_ROOT . "/../vendor");
