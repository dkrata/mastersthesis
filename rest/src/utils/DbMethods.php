<?php

namespace mastersthesis\utils\database;

use mastersthesis\utils\database\DbMethodsInterface;
use mastersthesis\models\Article;

class DbMethods implements DbMethodsInterface
{
    private $connection;

    public function __construct(\PDO $connection)
    {
        $this->connection = $connection;
    }

    public function addArticle(Article $article)
    {
        $query =
            'INSERT INTO articles (`author`, `title`, `content`, `size`)
            VALUES (:author, :title, :content, :size)';

        $this->connection
            ->prepare($query)
            ->execute(
                array(
                    ':author' => $article->getAuthor(),
                    ':title' => $article->getTitle(),
                    ':content' => $article->getContent(),
                    ':size' => $article->getSize()
                )
            );
    }

    public function getArticle($id)
    {
        $query = 'SELECT * FROM articles WHERE id = :id LIMIT 1';

        $statement = $this->connection
            ->prepare($query);
        $statement->execute(array(':id' => $id));

        $result = $statement->fetch(\PDO::FETCH_ASSOC);

        if (empty($result)) {
            return null;
        }

        $article = new Article();
        $article->setId($result['id'])
            ->setCreated($result['created'])
            ->setAuthor($result['author'])
            ->setTitle($result['title'])
            ->setContent($result['content'])
            ->setSize($result['size']);

        return $article;
    }

    public function getArticlesByLimitAndPage($limit, $page)
    {
        $start = ($page - 1) * $limit;

        $query = 'SELECT * FROM articles';

        $query .= ($limit > 0) ? ' LIMIT ' . $start . ', ' . $limit . ';' : ';';

        $statement = $this->connection
            ->prepare($query);
        $statement->execute();

        $results = $statement->fetchAll(\PDO::FETCH_ASSOC);

        if (empty($results)) {
            return null;
        }

        $articles = array();
        foreach ($results as $result) {
            $article = new Article();
            $article->setId($result['id'])
                ->setCreated($result['created'])
                ->setAuthor($result['author'])
                ->setTitle($result['title'])
                ->setSize($result['size']);
            $articles[] = $article;
        }

        return $articles;
    }

    public function updateArticle(Article $article)
    {
        $query =
            'UPDATE articles
            SET author=:author, title=:title, content=:content, size=:size
            WHERE id = :id';

        $statement = $this->connection
            ->prepare($query);

        $statement->execute(
            array(
                ':id' => $article->getId(),
                ':author' => $article->getAuthor(),
                ':title' => $article->getTitle(),
                ':content' => $article->getContent(),
                ':size' => $article->getSize()
            )
        );
    }

    public function deleteArticle($id)
    {
        $query = 'DELETE FROM articles WHERE id = :id LIMIT 1';

        $statement = $this->connection
            ->prepare($query);
        $statement->execute(array(':id' => $id));
    }
}
