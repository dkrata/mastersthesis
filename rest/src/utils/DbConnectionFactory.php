<?php

namespace mastersthesis\utils\database;

/**
* Sets and return one instance of dbConnection
*/
class DbConnectionFactory
{
    private $dbConfig;
    private $dbConnection;

    public function __construct(array $config)
    {
        $this->dbConfig = $config;
    }

    /**
    * Returns newly created or existing instance of PDO db connection
    *
    * @return PDO
    *   Instance of PDO connection
    */
    public function getDbConnection()
    {
        if (empty($this->dbConnection)) {
            $dsn =
                $this->dbConfig['driver'] .
                ':dbname=' .
                $this->dbConfig['database_name'] .
                ';host=' .
                $this->dbConfig['host'];
            $this->dbConnection =
                new \PDO(
                    $dsn,
                    $this->dbConfig['credentials']['admin']['username'],
                    $this->dbConfig['credentials']['admin']['password']
                );
        }

        return $this->dbConnection;
    }
}
