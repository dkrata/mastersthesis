<?php

namespace mastersthesis\utils\database;

class DbSchemaCreator
{
    private $dbConfig;

    /**
     * Constructor of DbSchemaCreator. Throws exception if schema name is empty.
     *
     * @param \PDO   $connection
     *        database connection
     * @param string $schemaName
     *        Name of schema.
     */
    public function __construct($dbConfig)
    {
        $this->dbConfig = $dbConfig;
    }

    /**
     * Creates schema
     */
    public function createSchema()
    {

        $user = "'" . $this->dbConfig['credentials']['admin']['username'] . "'@'localhost'";

        $query_db = "CREATE SCHEMA `" . $this->dbConfig['database_name'] . "` CHARACTER SET utf8 COLLATE utf8_general_ci;";
        $query_user = "CREATE USER $user IDENTIFIED BY '" . $this->dbConfig['credentials']['admin']['password'] ."';";
        $query_privileges = "GRANT ALL ON `" . $this->dbConfig['database_name'] . "`.* TO $user;";
        $query_flush = "FLUSH PRIVILEGES;";

        $pdo = $this->getRootConnection();
        $pdo->exec($query_db);
        $pdo->exec($query_user);
        $pdo->exec($query_privileges);
        $pdo->exec($query_flush);
    }

    /**
     * Drops schema
     */
    public function dropSchema()
    {
        $query = '
            DROP SCHEMA ' . $this->dbConfig['database_name'] . ';
        ';

        $pdo = $this->getRootConnection();
        $pdo->exec($query);
    }

    /**
     * Creates tables
     */
    public function createTables()
    {
        $query = '
            USE ' . $this->dbConfig['database_name'] . ';
            CREATE TABLE articles (
                id INT AUTO_INCREMENT PRIMARY KEY,
                author VARCHAR(50),
                title VARCHAR(100),
                content TEXT,
                size BIGINT,
                created TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
            );
        ';

        $pdo = $this->getRootConnection();
        $pdo->exec($query);
    }

    /**
     * Returns db connection of root user
     *
     * @return PDO dbConnection
     */
    private function getRootConnection()
    {

        $dsn = $this->dbConfig['driver'] .
            ':host=' .
            $this->dbConfig['host'];

        return
            new \PDO(
                $dsn,
                $this->dbConfig['credentials']['root']['username'],
                $this->dbConfig['credentials']['root']['password']
            );
    }
}
