<?php

namespace mastersthesis\utils\database;

use mastersthesis\models\Article;

interface DbMethodsInterface
{
    /**
     * Adds article to database
     *
     * @param Article $article
     *        Article to add
     */
    public function addArticle(Article $article);
    /**
     * Gets article from database
     *
     * @param  int $id
     *         Article's id
     *
     * @return Article
     *         Article if found, otherwise null
     */
    public function getArticle($id);
    /**
     * Gets articles by limit and page
     *
     * @param  int $limit
     *         Limit of articles per page
     *
     * @param  int $page
     *         Page of articles
     *
     * @return array of Articles
     *         Returns array of articles if found.
     */
    public function getArticlesByLimitAndPage($limit, $page);
    /**
     * Updates article in database
     *
     * @param  Article $article
     *         Updated article
     *
     * @return Article
     *         Updated article
     */
    public function updateArticle(Article $article);
    /**
     * Deletes article from database
     *
     * @param  int $id
     *         Id of article to delete
     */
    public function deleteArticle($id);
}
