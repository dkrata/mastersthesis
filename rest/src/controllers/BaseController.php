<?php

namespace mastersthesis\controllers;

use mastersthesis\utils\database\DbMethodsInterface;

class BaseController
{
    protected $silexApp;
    protected $dbMethods;

    public function __construct(\Silex\Application &$app, DbMethodsInterface $dbMethods)
    {
        $this->silexApp = $app;
        $this->dbMethods = $dbMethods;
    }
}
