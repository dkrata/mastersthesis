<?php

namespace mastersthesis\controllers;

use mastersthesis\models\Article;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ArticleController extends BaseController
{
    private static $defaultArticlesLimit = 0;

    public function registerControllers()
    {
        $this->registerGetArticles();
        $this->registerGetArticle();
        $this->registerPostArticle();
        $this->registerPutArticle();
        $this->registerDeleteArticle();
    }

    private function registerGetArticle()
    {
        $this->silexApp->get('/articles/{id}', function ($id) {
            $id = intval($id);

            $article = $this->dbMethods->getArticle($id);
            if (empty($article)) {
                return new Response('Article is not found', 404);
            }

            return $this->silexApp->json($article);
        });
    }

    private function registerGetArticles()
    {
        $this->silexApp->get('/articles/', function () {
            $articles =
                $this
                    ->dbMethods
                    ->getArticlesByLimitAndPage(ArticleController::$defaultArticlesLimit, 1);

            return $this->silexApp->json($articles);
        });
    }

    private function registerPostArticle()
    {
        $this->silexApp->post('/articles/', function (Request $request) {

            $data = $request->getContent();
            $article = $this->jsonToArticle($data);

            if (empty($article)) {
                return new Response('Article is wrong!', 404);
            }

            $this->dbMethods->addArticle($article);

            return new Response('Article is succesfully added!', 201);
        });
    }

    private function registerPutArticle()
    {
        $this->silexApp->put('/articles/', function (Request $request) {
            $data = $request->getContent();
            $article = $this->jsonToArticle($data);

            if (empty($article)) {
                error_log("Article is incorrect, is empty");
                return new Response('Article is wrong', 404);
            }

            $article_exist = !empty($this->dbMethods->getArticle($article->getId()));
            if (!$article_exist) {
                error_log("Article does not exist cannot update");
                return new Response('Article not found', 404);
            }

            $this->dbMethods->updateArticle($article);

            return new Response('Articles is successfully updated!', 201);
        });
    }

    private function registerDeleteArticle()
    {
        $this->silexApp->delete('/articles/{id}', function ($id) {
            $id = intval($id);
            if (!is_integer($id)) {
                return new Response('Invalid value of parameter id', 404);
            }

            $article_exist = !empty($this->dbMethods->getArticle($id));
            if (!$article_exist) {
                return new Response('Article does not exist', 404);
            }

            $this->dbMethods->deleteArticle($id);

            return new Response('Article is succesfully deleted!', 200);
        });
    }

    private function jsonToArticle($data)
    {
        $object = json_decode($data);

        if (
            !property_exists($object, 'id') ||
            !property_exists($object, 'created') ||
            !property_exists($object, 'author') ||
            !property_exists($object, 'title') ||
            !property_exists($object, 'content')||
            !property_exists($object, 'size')
        ) {
            return null;
        }

        $article = new Article();
        $article
            ->setId($object->id)
            ->setCreated($object->created)
            ->setAuthor($object->author)
            ->setTitle($object->title)
            ->setContent($object->content);
        $size = strlen($article->getAuthor() . "\n" . $article->getContent() . "\n");
        $article->setSize($size);

        return $article;
    }
}
