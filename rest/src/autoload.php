<?php

require 'settings/settings.php';

require DIR_VENDOR . '/autoload.php';

require DIR_MODELS . '/Article.php';

require DIR_UTILS . '/DbConnectionFactory.php';
require DIR_UTILS . '/interfaces/DbMethodsInterface.php';
require DIR_UTILS . '/DbMethods.php';
require DIR_UTILS . '/DbSchemaCreator.php';

require DIR_CONTROLLERS . '/BaseController.php';
require DIR_CONTROLLERS . '/ArticleController.php';
