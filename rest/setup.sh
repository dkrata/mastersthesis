#!/bin/bash

source_directory=`dirname $0`"/src"
settings_directory=$source_directory"/settings"
db_settings=$settings_directory/"db_settings.php"
php_settings=$settings_directory/"settings.php"

function main {
    display_header
    case $1 in
        install)
            install_database
            ;;
        reset)
            reset_database
            ;;
        mock)
            mock_database
            ;;
        *)
            display_error "Wrong parameter is used. Please check usage."
            display_help
            ;;
    esac
}

function separate_section {
    echo $'\n++++++++++++++++++++++++++++++++++++++++\n'
}

function display_header {
    cat << EOF

#################################################
###### Master's Thesis REST service setup #######
#################################################

EOF
}

function display_help {
    cat << EOF
Usage: $(basename "$0") [ OPTION ]

where OPTION can be:
    install  -  full installation of mastersthesis (settings and database)
    reset    -  drops and recreates database using existing database properties
    mock     -  adds mock entries to database

EOF
}

function create_settings_files {
    db_settings_file=$settings_directory"/default_db_settings.php"
    php_settings_file=$settings_directory"/default_settings.php"
    if [ -e "$db_settings_file" ]
    then
        cp $settings_directory/{default_,}db_settings.php
    else
        display_error "File $db_settings_file does not exist in $settings_directory."
        exit 1
    fi
    if [ -e "$php_settings_file" ]
    then
        cp $settings_directory/{default_,}settings.php
    else
        display_error "File $settings_file does not exist in $settings_directory."
        exit 1
    fi
}

function get_php_parameters {
    return
}

function set_php_parameters {
    return
}

function get_database_parameters {
    database_driver="mysql"
    printf 'Enter database name (default: mastersthesis): '
    read database_name
    database_name=${database_name:-mastersthesis}
    printf 'Enter database host (default: localhost): '
    read database_host
    database_host=${database_host:-localhost}
    printf 'Enter database port (default: 3306): '
    read database_port
    database_port=${database_port:-3306}
    printf 'Enter root login: '
    read root_login
    printf 'Enter root password: '
    read -s root_password
    printf '\nEnter database admin login: '
    read admin_login
    printf 'Enter database admin password: '
    read -s admin_password
    echo
}

function set_database_settings {
    sed -i "s/{database\.name}/$database_name/" $db_settings
    sed -i "s/{database\.driver}/$database_driver/" $db_settings
    sed -i "s/{database\.host}/$database_host/" $db_settings
    sed -i "s/{database\.port}/$database_port/" $db_settings
    sed -i "s/{database\.root\.username}/$root_login/" $db_settings
    sed -i "s/{database\.root\.password}/$root_password/" $db_settings
    sed -i "s/{database\.admin\.username}/$admin_login/" $db_settings
    sed -i "s/{database\.admin\.password}/$admin_password/" $db_settings
}

function run_database_mock_initializer {
    echo $'Mocking...'
    php $source_directory/installer.php --mock-database
}

function run_database_installer {
    echo $'Performing database queries'
    php $source_directory/installer.php --install
    echo $'Database queries finished'
}

function install_database {
    echo 'Database installation'
    separate_section
    create_settings_files
    get_database_parameters
    set_database_settings
    get_php_parameters
    set_php_parameters
    separate_section
    run_database_installer
    separate_section
    echo 'Database installation is finished'
}

function reset_database {
    echo 'Recreating database schema'
    separate_section
    run_database_installer
    separate_section
    echo 'Recreating is finished'
}

function mock_database {
    echo 'Adding mock entries to database'
    separate_section
    run_database_mock_initializer
    separate_section
    echo 'Mock entries added'
}

function display_error {
    echo
    cat <<< "[Error] $@" 1>&2
    echo
}

main $@
exit 0