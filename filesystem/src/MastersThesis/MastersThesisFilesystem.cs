using System;
using System.Collections.Generic;
using Mono.Fuse;
using Mono.Unix.Native;
using MastersThesis.WebApiWrappers;
using MastersThesis.Models;
using System.Linq;
using System.Text.RegularExpressions;
using System.Text;

namespace MastersThesis
{
	public class MastersThesisFilesystem : FileSystem
	{
		#region Properties
		private IDictionary<string, Article> _articles = new Dictionary<string, Article> ();
		private IDictionary<string, FileArticle> _newArticlesQueue = new Dictionary<string, FileArticle> ();
		private IArticlesWrapper _articlesWrapper = new ArticlesWrapper();
		private bool _updateLock = false;
		#endregion

		#region Constructor
		public MastersThesisFilesystem (string[] args) : base (args)
		{
			Logger.Debug (String.Format ("Initialization. args: {0}. ", string.Join(@" | ", args)));
			ReloadArticles ();
		}
		#endregion

		#region FileSystem methods overrided

		protected override Errno OnReadDirectory (string directory, OpenedPathInfo info, out IEnumerable<DirectoryEntry> names)
		{
			Logger.Debug (String.Format("Directory: [{0}]",directory));

			if (directory != "/") {
				names = null;
				Logger.Debug("Directory is not frontslash. No entries");
				return Errno.ENOENT;
			}
			names = ListNames (directory);
			Logger.Debug("Names assigned - return 0");
			return 0;
		}

		protected override Errno OnAccessPath (string path, AccessModes mode)
		{
			Logger.Debug (String.Format("path[{0}], mode[{1}]", path, mode));
			if (!_articles.ContainsKey (path)) {
				Logger.Debug("No file on path");
				return Errno.ENOENT;
			}

			return 0;
		}

		/// <summary>
		/// Returns path status. 
		/// !IMPORTANT! Path status properties are taken when file is opened (e.g. file size).
		/// </summary>
		/// <param name="path">Path.</param>
		/// <param name="stbuf">Stbuf.</param>
		protected override Errno OnGetPathStatus (string path, out Stat stbuf)
		{
			LogCurrentArticles ();

			Logger.Debug (String.Format("path[{0}]",path));
			stbuf = new Stat ();
			if (path == "/") {
				stbuf.st_mode  = GetMastersthesisMode(true);
				stbuf.st_nlink = 1;
				Logger.Debug("Main path - return 0");
				return 0;
			}

			if (!_articles.ContainsKey (path)) {
				var articleQueued = _newArticlesQueue.FirstOrDefault (x => x.Key == path).Value;
				if (articleQueued != null && articleQueued.isLocked) {
					Logger.Debug("Article exists but is locked - return errno.EACCESS");
					return Errno.EACCES;
				}

				Logger.Debug("No article - return errno.ENOENT");
				return Errno.ENOENT;
			}

			var article = _articles [path];

			stbuf.st_mode = GetMastersthesisMode();
			stbuf.st_size = article.Size;
			stbuf.st_mtime = article.Created.ConvertToUnixTimestamp();

			Logger.Debug("Article found - return 0");
			return 0;
		}

		protected override Errno OnGetHandleStatus (string file, OpenedPathInfo info, out Stat buf)
		{
			Logger.Debug (String.Format("file[{0}],info[{1}]", file, info.ToString()));
			LogCurrentArticles ();
			buf = new Stat ();
			if (!SetFileStat (file, ref buf)) {
				Logger.Debug("Return errno.ENOENT");
				return Errno.ENOENT;
			}
			Logger.Debug("Return 0");
			return 0;
		}

		protected override Errno OnCreateHandle (string file, OpenedPathInfo info, FilePermissions mode)
		{
			Logger.Debug (String.Format ("file[{0}], info[{1}], mode[{2}]", file, info.ToString(), mode.ToString()));
			if (!_articles.ContainsKey (file)) {
				Logger.Debug("Article not exist - creating new article");

				var title = file.Split (new string[]{ @"/" }, StringSplitOptions.None) [1];
				var article = new Article () { Title = title, Created = DateTime.UtcNow };
				_newArticlesQueue.Add (file, new FileArticle() { Article = article });
				return 0;
			}
			Logger.Debug("Article exist - return errno.EEXIST");
			return Errno.EEXIST;
		}

		protected override Errno OnOpenHandle (string file, OpenedPathInfo info)
		{
			Logger.Debug(string.Format ("file[{0}], info_OpenFlags[{1}]", file, info.OpenFlags));
			if (!_articles.ContainsKey (file)) {
				Logger.Debug("No article - return errno.ENOENT");
				return Errno.ENOENT;
			}
			if (info.OpenAccess != OpenFlags.O_RDONLY && info.OpenAccess != OpenFlags.O_WRONLY) {
				Logger.Debug("No access - return errno.EACCESS");
				return Errno.EACCES;
			}
			return 0;
		}

		protected override Errno OnReadHandle (string file, OpenedPathInfo info, byte[] buf, long offset, out int bytesWritten)
		{
			Logger.Debug (String.Format("Offset[{0}], File: [{1}]", offset, file));
			bytesWritten = 0;
			int size = buf.Length;
			if (_articles.ContainsKey (file)) {
				var article = _articlesWrapper.GetArticle(_articles.First (x => x.Key == file).Value.Id);
				var content = article.GetArticleContent();
				byte[] source = Encoding.UTF8.GetBytes(content);
				if (offset < (long) source.Length) {
					if (offset + (long) size > (long) source.Length)
						size = (int) ((long) source.Length - offset);
					Logger.Debug (string.Format ("BLOCK COPY - size[{0}], offset[{1}], length[{2}], contentLength[{3}]", size, offset, source.Length, content.Length));
					Buffer.BlockCopy (source, (int) offset, buf, 0, size);
				} 
				else
					size = 0;
			} 
			else 
				return Errno.ENOENT;

			bytesWritten = size;
			Logger.Debug (String.Format("Bytes written: [{0}]",bytesWritten));
			return 0;
		}

		protected override Errno OnWriteHandle (string file, OpenedPathInfo info, byte[] buf, long offset, out int bytesRead)
		{
			_updateLock = true;
			bytesRead = 0;
			var content = Encoding.UTF8.GetString (buf);

			Logger.Debug (String.Format ("file[{0}], content[{1}]", file, content));

			string[] splitCharacters = { Environment.NewLine	 };
			var contentSplitted = content.Split(splitCharacters, StringSplitOptions.None);

			if (contentSplitted.Length < 1) {
				Console.Error.WriteLine ("Article is incorrect. Should be in format: Author, Content separated by newline.");
				Logger.Debug("Bad entry (less than 2 lines) - return EBADE");
				return Errno.EBADE;
			}

			for (int i = 0; i < 2; i++) {
				if (contentSplitted [i].Length < 1 || String.IsNullOrWhiteSpace (contentSplitted [i])) 
				{
					Console.Error.WriteLine ("Article is incorrect. Author and content should not be empty.");
					Logger.Debug("Bad entry - One of first two lines is null - return EBADE");
					return Errno.EBADE;
				}
			}

			var article = _articles.FirstOrDefault(x => x.Key == file).Value;
			if (article == null) 
			{
				article = _newArticlesQueue.First (x => x.Key == file).Value.Article;
			}

			article.Author = contentSplitted [0];
			article.Content = string.Join (Environment.NewLine, contentSplitted.Skip(1));

			Logger.Debug(String.Format("Joined content: [{0}]", article.Content));


			bytesRead = buf.Length;
			Logger.Debug("Return 0");
			return 0;
		}

		protected override Errno OnFlushHandle (string file, OpenedPathInfo info)
		{
			//WHEN ARTICLE IS IN READ MODE THEN FLUSH IS RUNNING TOO!!!

			Logger.Debug("Updating article in database... [" + file +"]");
			var article = _articles.FirstOrDefault(x => x.Key == file).Value;
			if (article == null) {
				Logger.Debug("Article not exist - new created");
				article = _newArticlesQueue.First (x => x.Key == file).Value.Article;
				article.Id = 0;
				Logger.Debug(String.Format("Article to save - title[{0}], author[{1}]", article.Title, article.Author));
				try{
					_articlesWrapper.Post (article); 
					_newArticlesQueue.Remove(file);
					Logger.Debug (String.Format ("Article [{0}] has been succesfully saved.", article.Title));
				} catch (Exception) { 
					_newArticlesQueue.Remove(file);
					Console.Error.WriteLine ("Cannot save an article - an error occured with connection");
					return Errno.EIO;
				}

			}
			else if (_updateLock)
			{
				Logger.Debug("Article exist - change");
				try{
					_articlesWrapper.Put (article);
					Logger.Debug (String.Format ("Article [{0}] has been succesfully updated.", article.Title));
				} catch (Exception) {
					Console.Error.WriteLine ("Cannot save an article - an error occured with connection");
					return Errno.EIO; 
				}
			}

			return 0;
		}

		protected override Errno OnReleaseHandle (string file, OpenedPathInfo info)
		{
			_updateLock = false;
			ReloadArticles ();
			Logger.Debug (String.Format("file[{0}],info[{1}]", file, info.Handle.ToString()));
			return 0;
		}

		protected override Errno OnRemoveFile (string file)
		{
			Logger.Debug (String.Format ("file[{0}]", file));
			var article = _articles.FirstOrDefault (x => x.Key == file).Value;
			if (article == null) {
				Logger.Debug("Article not found - return ENOENT");
				return Errno.ENOENT;
			}
			_articlesWrapper.Delete (article.Id);
			Logger.Debug("Article deleted, return 0");
			ReloadArticles ();

			return 0;
		}

		protected override Errno OnCreateSpecialFile (string file, FilePermissions perms, ulong dev)
		{
			Logger.Debug (String.Format("file[{0}], perms[{1}], dev[{2}]",file, perms.ToString(), dev));
			return 0;
		}

		protected override Errno OnTruncateFile (string file, long length)
		{
			Logger.Debug (String.Format("file[{0}], length[{1}]", file, length));
			return 0;
		}
		#endregion

		#region Helpers

		private void LogCurrentArticles()
		{
			Logger.Debug (String.Format("Current articles: [{0}]", string.Join("] [", _articles.Keys)));
		}

		private FilePermissions GetMastersthesisMode(bool isDirectory = false)
		{
			if (isDirectory) 
				return NativeConvert.FromUnixPermissionString ("dr-xr-xr-x");
			return NativeConvert.FromUnixPermissionString ("-rw-rw-r--");
		}

		/// <summary>
		/// Sets the file stat.
		/// </summary>
		/// <returns><c>true</c>, if file stat was set, <c>false</c> otherwise.</returns>
		/// <param name="path">Path.</param>
		/// <param name="buf">Buffer.</param>
		private bool SetFileStat (string path, ref Stat buf)
		{
			if (path == "/") 
			{
				Logger.Debug("Main path");
				buf.st_mode = GetMastersthesisMode (true);
				return false;
			} 
			else 
			{
				Logger.Debug("Not main path");
				buf.st_mode = GetMastersthesisMode();
			}

			var article = _articles.FirstOrDefault (x => x.Key == path).Value;
			if (article == null) {
				Logger.Debug("Article exist");
				article = _newArticlesQueue.FirstOrDefault (x => x.Key == path).Value.Article;
			}

			if (article != null) 
			{
				Logger.Debug("Article exist, is newly created");
				buf.st_size = article.Size;
				buf.st_mtime = article.Created.ConvertToUnixTimestamp ();

				return true;
			}


			Logger.Debug("Article not exist");
			return false;
		}

		/// <summary>
		/// Method used to reload internal article's list.
		/// </summary>
		private void ReloadArticles()
		{
			Logger.Debug ("Reload articles");
			_articles = _articlesWrapper
				.GetArticles ()
				.ToDictionary(
					x => String.Format ("/{0}_{1}", x.Id, x.Title),
					x => x
				);
		}

		protected IEnumerable<DirectoryEntry> ListNames (string directory)
		{
			foreach (var name in _articles.Keys) {
				yield return new DirectoryEntry (name.Substring(1));
			}
		}
		#endregion

		#region FileSystem methods overrided - not important

		protected override Errno OnGetPathExtendedAttribute (string path, string name, byte[] value, out int bytesWritten)
		{
			Logger.Debug ();
			return base.OnGetPathExtendedAttribute (path, name, value, out bytesWritten);
		}

		protected override Errno OnLockHandle (string file, OpenedPathInfo info, FcntlCommand cmd, ref Flock @lock)
		{
			Logger.Debug (String.Format("file[{0}], info[{1}], cmd[{2}], return 0", file, info.ToString(),cmd.ToString()));
			return 0;
		}

		protected override Errno OnReleaseDirectory (string directory, OpenedPathInfo info)
		{
			Logger.Debug (String.Format("directory[{0}]", directory));
			return 0;
		}

		protected override Errno OnRemoveDirectory (string directory)
		{
			Logger.Debug (String.Format("directory[{0}]", directory));
			return base.OnRemoveDirectory (directory);
		}

		#endregion

		#region Main
		public static void Main (string[] args)
		{
			Logger.Debug ("\n\nFS - Start new session");
			using (MastersThesisFilesystem fs = new MastersThesisFilesystem (args)) {
				fs.MultiThreaded = false;
				fs.Start ();
			}
			Logger.Debug ("Closing app");
		}
		#endregion
	}
}

