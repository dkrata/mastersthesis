using System;
using System.IO;
using System.Diagnostics;

namespace MastersThesis
{
	public static class Logger
	{
		private static string _filename = @"log.txt";
		private static long _milisecondsToElapse = 150;
		private static Stopwatch _stopwatch = new Stopwatch();

		static Logger ()
		{
			_stopwatch.Start ();
		}

		public static void Info(string message = "")
		{
			StackTrace stackTrace = new StackTrace();
			var methodName = stackTrace.GetFrame(1).GetMethod().Name;

			AppendLog (methodName, message, LogLevel.Info);
		}

		public static void Debug(string message = "")
		{
			StackTrace stackTrace = new StackTrace();
			var methodName = stackTrace.GetFrame(1).GetMethod().Name;

			AppendLog (methodName, message, LogLevel.Debug);
		}

		private static string LogLevelString(LogLevel logLevel)
		{
			switch (logLevel) 
			{
				case LogLevel.Info:
					return "info";
				case LogLevel.Debug:
					return "debug";
				default:
					return null;
			}
		}

		private static bool IsTimeElapsed()
		{
			bool elapsed = false;
			if (_stopwatch.ElapsedMilliseconds >= _milisecondsToElapse) 
			{
				elapsed = true;
			}
			_stopwatch.Restart ();
			return elapsed;
		}

		private static void AppendLog(string methodName, string message, LogLevel logLevel)
		{
			string logLevelString = LogLevelString (logLevel);
			using (StreamWriter writer = new StreamWriter(_filename, true))
			{
				string newLines = "";
				if (IsTimeElapsed ()) {
					newLines = string.Concat(newLines, Environment.NewLine, Environment.NewLine);
				}
				writer.WriteLine ("{0}[{1}][{2}][{3}] - {4}", newLines, DateTime.Now, logLevelString, methodName, message);
				
				writer.Flush();
				writer.Close ();
			}
		}
	}


}
