using System;

namespace MastersThesis
{
	/// <summary>
	/// Interface of logger.
	/// </summary>
	public interface ILogger
	{
		/// <summary>
		/// Adds 'Info' level log to log file.
		/// </summary>
		void Info(string message);

		/// <summary>
		/// Adds 'Debug' level log to log file.
		/// </summary>
		void Debug(string message);
	}

	/// <summary>
	/// Log level of logs.
	/// </summary>
	public enum LogLevel
	{
		Info,
		Debug
	}
}

