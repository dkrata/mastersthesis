using System.Threading.Tasks;
using System.Collections.Generic;
using System;

namespace MastersThesis.Models
{
	public class Article
	{
		public int Id { get; set; }
		public DateTime Created { get; set; }
		public string Author { get; set; }
		public string Title { get; set; }
		public string Content { get; set; }
		public long Size { get; set; }
	}

	public class FileArticle
	{
		public Article Article { get; set; }
		public bool isLocked { get; set; }

		public FileArticle ()
		{
			isLocked = false;
		}
	}
	
	public static class ArticleExtensions
	{
		public static string GetArticleContent(this Article article)
		{
			return String.Format ("{1}{0}{2}{0}", Environment.NewLine, article.Author, article.Content);
		}
	}
}

