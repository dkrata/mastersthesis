using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MastersThesis.Models;
using Newtonsoft.Json;
using System.Net;
using System.Linq;
using Newtonsoft.Json.Converters;
using System.Text;
using System.IO;

namespace MastersThesis.WebApiWrappers
{
	public class ArticlesWrapper : IArticlesWrapper
	{
		private readonly string _address = "http://localhost:8083/articles/";

		protected WebClient GetClient()
		{
			return new WebClient();
		}

		#region IArticlesWrapper implementation
		public IEnumerable<Article> GetArticles()
		{
			Logger.Debug ("Getting all articles");
			using (var client = GetClient ())
			{
				var result = client.DownloadString(new Uri(_address));
				IEnumerable<Article> articles;

				articles = (result == null || result == "{}") 
					? new List<Article> () 
					: articles = JsonConvert.DeserializeObject<IEnumerable<Article>> (result, new IsoDateTimeConverter());
					
				return articles;
			}
		}

		public Article GetArticle (int id)
		{
			Logger.Debug (String.Format("Getting article with id[{0}]", id));
			using (var client = GetClient ())
			{
				var result = client.DownloadString(new Uri(_address + id));
				var article = JsonConvert.DeserializeObject<Article> (result, new IsoDateTimeConverter());

				return article;
			}
		}

		public void Post (Article article)
		{
			Logger.Debug (String.Format("Posting article with title [{0}] and author[{1}]", article.Title, article.Author));
			using (var client = GetClient ())
			{
				try{
					var json = ConvertArticleToJson(article);
					Logger.Debug(String.Format("POST Json: [{0}], url[{1}]", json, _address));

					client.Headers[HttpRequestHeader.ContentType] = "application/json";
					var response = client.UploadString(new Uri(_address), json);

					response.ToString ();
				}catch(WebException e) {
					Logger.Debug ("Exception: " + e.Message);
					throw e;
				}
			}
		}

		public void Put (Article article)
		{
			Logger.Debug (String.Format("Putting article with title [{0}] and author[{1}]", article.Title, article.Author));
			using (var client = GetClient ())
			{
				try{
					var json = ConvertArticleToJson(article);

					client.Headers[HttpRequestHeader.ContentType] = "application/json";
					var response = client.UploadString(new Uri(_address), "PUT", json);

					response.ToString ();
				}catch(Exception e) {
					Logger.Debug ("Exception: " + e.Message);
					throw e;
				}
			}
		}

		public void Delete (int id)
		{
			Logger.Debug (String.Format("Deleting article with id [{0}]", id));
			using (var client = GetClient ())
			{
				client.UploadString (_address + id, "DELETE", String.Empty);
			}
		}

		private string ConvertArticleToJson(Article article)
		{
			var articleObject = new { 
				id = article.Id, 
				created = article.Created, 
				author = article.Author, 
				title = article.Title, 
				content = article.Content, 
				size = article.Size
			};

			return JsonConvert.SerializeObject(articleObject, new IsoDateTimeConverter());

		}
		#endregion
	}
}

