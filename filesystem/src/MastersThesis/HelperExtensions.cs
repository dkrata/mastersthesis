using System;

namespace MastersThesis
{
	public static class HelperExtensions
	{
		public static long ConvertToUnixTimestamp(this DateTime date)
		{
			return (long)(date - new DateTime (1970, 1, 1)).TotalSeconds;
		}

	}
}

