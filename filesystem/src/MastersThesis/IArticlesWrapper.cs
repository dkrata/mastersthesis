using System.Threading.Tasks;
using System.Collections.Generic;
using MastersThesis.Models;

namespace MastersThesis.WebApiWrappers
{
	public interface IArticlesWrapper
	{
		IEnumerable<Article> GetArticles();
		Article GetArticle(int id);
		void Post(Article article);
		void Put(Article article);
		void Delete (int id);
	}
}

